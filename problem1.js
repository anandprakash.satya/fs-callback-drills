const fs = require("fs");
const path = require("path");

function problem1() {
  fs.mkdir(path.join(__dirname, "intro"), (err) => {
    if (err) throw err;
     else {
      console.log("Directory created");
      fs.writeFile("./intro/name.json", '{"name":"Anand"}', function (err) {
        if (err) throw err;
        else {
          console.log("file created");
          fs.unlink("./intro/name.json", (err) => {
            console.log("file deleted");
          });
        }
      });
    }
  });
}
module.exports = problem1;