const fs = require("fs");
const path = require("path");

function problem2() {
  
  fs.readFile('./lipsum.txt','utf-8',(err,data)=>{
    if(err){
      console.log(err);
    }else{
      console.log('Accessed file');
      let upper_case=data.toUpperCase()
      fs.writeFile('./uppercase.txt',upper_case,(err)=>{
        if (err) throw err
        else{
          console.log('written file');
          fs.appendFile('./filenames.txt','uppercase.txt',(err)=>{
            if (err) throw err
            else{
              fs.readFile('./lipsum.txt','utf-8',(err,data)=>{
                if(err) throw err
                else{
                  let lower_case=data.toLowerCase().split('.').join('.\n')
                  fs.writeFile('./lowercase.txt',lower_case,(err)=>{
                    if(err) throw err
                    else{
                      fs.appendFile('./filenames.txt','&lowercase.txt',(err)=>{
                        if(err) throw err
                        else{
                          fs.readFile('./uppercase.txt','utf-8',(err,data)=>{
                            if(err) throw err
                            else{
                              const data1=data
                              fs.readFile('./lowercase.txt','utf-8',(err,data)=>{
                                if(err) throw err
                                else{
                                  const data2=data
                                  const concat=data1+data2
                                  const sorted=(concat.split('\n').join('').split(' ').join('').split('.').join('').split('').sort().join(''))
                                  fs.writeFile('./sorted.txt',sorted,(err)=>{
                                    if(err) throw err
                                    else{
                                      fs.appendFile('./filenames.txt','&sorted.txt',(err)=>{
                                        if(err) throw err
                                        else{
                                          fs.readFile('./filenames.txt','utf-8',(err,data)=>{
                                            if(err) throw err
                                            else{
                                              const arr = data.split('&')
                                              for(let i=0;i<arr.length;i++){
                                                fs.unlink(`./${arr[i]}`,(err)=>{ 
                                                  if(err) throw err
                                                  console.log('files deleted');
                                                });
                                              }
                                            }
                                          })
                                        }
                                      })
                                    }
                                  })
                                }
                              })
                            }
                          })
                        }
                      })

                    }
                  })
                }
              })
            }
          })
        }
      })
    }
  })
}

  module.exports = problem2

